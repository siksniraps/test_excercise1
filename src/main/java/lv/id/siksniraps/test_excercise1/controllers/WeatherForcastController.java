package lv.id.siksniraps.test_excercise1.controllers;

import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import lv.id.siksniraps.test_excercise1.entities.WeatherForcast;
import lv.id.siksniraps.test_excercise1.services.external.IpApiGeolocationService;
import lv.id.siksniraps.test_excercise1.services.external.OpenWeatherMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class WeatherForcastController {

    private final IpApiGeolocationService ipApiGeolocationService;
    private final OpenWeatherMapService openWeatherMapService;

    @Autowired
    public WeatherForcastController(
            IpApiGeolocationService ipApiGeolocationService,
            OpenWeatherMapService openWeatherMapService) {
        this.ipApiGeolocationService = ipApiGeolocationService;
        this.openWeatherMapService = openWeatherMapService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/weather")
    public Map<String, Object> getWeather(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        GeoLocation location = ipApiGeolocationService.getLocationFromIp(ip);
        WeatherForcast forcast = openWeatherMapService.getWeatherForcast(location);

        Map<String, Object> result = new HashMap<>();
        result.put("location", location);
        result.put("weather", forcast);
        return result;
    }

}
