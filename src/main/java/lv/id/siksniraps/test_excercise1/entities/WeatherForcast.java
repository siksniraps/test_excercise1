package lv.id.siksniraps.test_excercise1.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForcast {

    private Double latitude;
    private Double longitude;
    private String city;
    private String country;
    private Double temperature;
    private Double pressure;
    private Double humidity;
    private Double maxTemperature;
    private Double minTemperature;
    private Double windSpeed;
    private Double windDirection;
    private Double clouds;
    private Double rain;
    private Double snow;
    private String description;
    private Date dateTime;

    public WeatherForcast() {}

    @JsonCreator
    public WeatherForcast(
            @JsonProperty("name") String name,
            @JsonProperty("coord") Map<String, Object> coords,
            @JsonProperty("sys") Map<String, Object> sys,
            @JsonProperty("weather") List<Map<String, Object>> weather,
            @JsonProperty("main") Map<String, Object> main,
            @JsonProperty("rain") Map<String, Object> rain,
            @JsonProperty("wind") Map<String, Object> wind,
            @JsonProperty("snow") Map<String, Object> snow,
            @JsonProperty("clouds") Map<String, Object> clouds,
            @JsonProperty("dt") Long dt) {

        city = name;

        if (coords != null) {
            latitude = (Double) coords.get("lat");
            longitude = (Double) coords.get("lon");
        }

        if (sys != null) {
            country = (String) sys.get("country");
        }


        if (weather != null) {
            description = (String) weather.get(0).get("description");
        }

        if (main != null) {
            temperature = Double.parseDouble(main.get("temp").toString());
            pressure = Double.parseDouble(main.get("pressure").toString());
            humidity = Double.parseDouble(main.get("humidity").toString());
            maxTemperature = Double.parseDouble(main.get("temp_max").toString());
            minTemperature = Double.parseDouble(main.get("temp_min").toString());

        }

        if  (wind != null) {
            windSpeed = Double.parseDouble(wind.get("speed").toString());
            windDirection = Double.parseDouble(wind.get("deg").toString());
        }
        this.clouds = Double.parseDouble(clouds.get("all").toString());
        if (rain != null) {
            this.rain = Double.parseDouble(rain.get("3h").toString());
        }

        if (snow != null) {
            this.snow = Double.parseDouble(snow.get("3h").toString());
        }


        if (dt != null) {
            dateTime = new Date(dt * 1000);
        }

    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Double getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(Double windDirection) {
        this.windDirection = windDirection;
    }

    public Double getClouds() {
        return clouds;
    }

    public void setClouds(Double clouds) {
        this.clouds = clouds;
    }

    public Double getRain() {
        return rain;
    }

    public void setRain(Double rain) {
        this.rain = rain;
    }

    public Double getSnow() {
        return snow;
    }

    public void setSnow(Double snow) {
        this.snow = snow;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherForcast that = (WeatherForcast) o;
        return Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(city, that.city) &&
                Objects.equals(country, that.country) &&
                Objects.equals(temperature, that.temperature) &&
                Objects.equals(pressure, that.pressure) &&
                Objects.equals(humidity, that.humidity) &&
                Objects.equals(maxTemperature, that.maxTemperature) &&
                Objects.equals(minTemperature, that.minTemperature) &&
                Objects.equals(windSpeed, that.windSpeed) &&
                Objects.equals(windDirection, that.windDirection) &&
                Objects.equals(clouds, that.clouds) &&
                Objects.equals(rain, that.rain) &&
                Objects.equals(snow, that.snow) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(latitude, longitude, city, country, temperature, pressure,
                humidity, maxTemperature, minTemperature, windSpeed, windDirection, clouds,
                rain, snow, dateTime, description);
    }

}
