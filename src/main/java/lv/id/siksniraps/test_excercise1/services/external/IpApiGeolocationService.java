package lv.id.siksniraps.test_excercise1.services.external;

import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class IpApiGeolocationService {

    @Value("${ip-api.url}")
    private String ipApiUrl;

    private final RestTemplate restTemplate;

    public IpApiGeolocationService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Cacheable(value = "weather", key = "#ip")
    public GeoLocation getLocationFromIp(String ip) {
        try {
            return restTemplate.getForObject(ipApiUrl + "/" + ip, GeoLocation.class);
        } catch (Exception e) {
            return null;
        }

    }
}
