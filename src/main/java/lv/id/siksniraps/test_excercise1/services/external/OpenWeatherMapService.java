package lv.id.siksniraps.test_excercise1.services.external;

import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import lv.id.siksniraps.test_excercise1.entities.WeatherForcast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Calendar;
import java.util.Date;

@Service
public class OpenWeatherMapService {

    @Value("${openweathermap.url}")
    private String openWeatherMapUrl;
    @Value("${openweathermap.api_key}")
    private String openWeatherMapApiKey;

    private final RestTemplate restTemplate;

    public OpenWeatherMapService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public WeatherForcast getWeatherForcast(GeoLocation location) {
        if (location != null && (location.getLatitude() == null || location.getLongitude() == null)) {
            return null;
        }
        try {
            return restTemplate.getForObject(
                    openWeatherMapUrl + "?" +
                            "lat=" + location.getLatitude() + "&" +
                            "lon=" + location.getLongitude() + "&" +
                            "units=metric&" +
                            "APPID=" + openWeatherMapApiKey, WeatherForcast.class);
        } catch (Exception e) {
            return null;
        }

    }


}
