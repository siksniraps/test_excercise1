package lv.id.siksniraps.test_excercise1;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.id.siksniraps.test_excercise1.controllers.WeatherForcastController;
import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import lv.id.siksniraps.test_excercise1.entities.WeatherForcast;
import lv.id.siksniraps.test_excercise1.services.external.IpApiGeolocationService;
import lv.id.siksniraps.test_excercise1.services.external.OpenWeatherMapService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(WeatherForcastController.class)
public class ApplicationTests {

	private MockMvc mockMvc;

	@Autowired
	private WeatherForcastController weatherForcastController;

	@Autowired
    private ObjectMapper mapper;

	@MockBean
    private IpApiGeolocationService ipApiGeolocationService;

	@MockBean
    private OpenWeatherMapService openWeatherMapService;

    private GeoLocation location;
    private WeatherForcast forcast;

    private static final String EXPECTED_RESPONSE = "{\"weather\":{\"rain\":10.0,\"snow\":11.0,\"clouds\":56.0,\"latitude\":57.3,\"longitude\":25.25,\"city\":\"Cesis\",\"country\":\"LV\",\"temperature\":17.26,\"pressure\":1012.45,\"humidity\":76.0,\"maxTemperature\":17.26,\"minTemperature\":17.26,\"windSpeed\":3.46,\"windDirection\":103.002,\"description\":\"broken clouds\",\"dateTime\":1530552012000},\"location\":{\"query\":\"185.61.150.209\",\"country\":\"Latvia\",\"regionName\":\"Cesu Novads\",\"city\":\"Cēsis\",\"zip\":\"LV-4126\",\"lat\":57.3,\"lon\":25.25}}";

	@Before
	public void setup() {
		this.mockMvc = standaloneSetup(weatherForcastController).build();
        location = new GeoLocation();
        location.setIp("185.61.150.209");
        location.setCountry("Latvia");
        location.setCity("Cēsis");
        location.setRegion("Cesu Novads");
        location.setZipCode("LV-4126");
        location.setLatitude(57.3);
        location.setLongitude(25.25);

        forcast = new WeatherForcast();
        forcast.setCity("Cesis");
        forcast.setDescription("broken clouds");
        forcast.setClouds(56.0);
        forcast.setCountry("LV");
        forcast.setDateTime(new Date(1530552012L * 1000));
        forcast.setHumidity(76.0);
        forcast.setPressure(1012.45);
        forcast.setTemperature(17.26);
        forcast.setMaxTemperature(17.26);
        forcast.setMinTemperature(17.26);
        forcast.setLatitude(57.3);
        forcast.setLongitude(25.25);
        forcast.setRain(10.0);
        forcast.setSnow(11.0);
        forcast.setWindSpeed(3.46);
        forcast.setWindDirection(103.002);
	}

	@Test
	public void contextLoads() {
        assertThat(weatherForcastController).isNotNull();
	}




	@Test
    public void weatherForcastApiTest() throws Exception {
	    when(ipApiGeolocationService.getLocationFromIp("185.61.150.209")).thenReturn(location);
        when(openWeatherMapService.getWeatherForcast(location)).thenReturn(forcast);
        String result = mockMvc.perform(
                get("/weather")
                        .with(request -> {
                            request.setRemoteAddr("185.61.150.209");
                            return request;
                        })
        ).andReturn().getResponse().getContentAsString();

        assertEquals(result, EXPECTED_RESPONSE);
    }

}
