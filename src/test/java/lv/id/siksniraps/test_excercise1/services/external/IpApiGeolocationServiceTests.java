package lv.id.siksniraps.test_excercise1.services.external;

import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.ExpectedCount.twice;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@RestClientTest(IpApiGeolocationService.class)
public class IpApiGeolocationServiceTests {
    @Autowired
    private IpApiGeolocationService ipApiGeolocationService;

    @Autowired
    private MockRestServiceServer mockServer;

    @Autowired
    private CacheManager cacheManager;

    private static final String CESIS_IP = "185.61.150.209";
    private static final String JSON_LOCATION_RESPONSE = "{\"as\":\"AS52173 Makonix SIA\",\"city\":\"Cēsis\",\"country\":\"Latvia\",\"countryCode\":\"LV\",\"isp\":\"Makonix SIA\",\"lat\":57.3,\"lon\":25.25,\"org\":\"Makonix SIA\",\"query\":\"185.61.150.209\",\"region\":\"022\",\"regionName\":\"Cesu Novads\",\"status\":\"success\",\"timezone\":\"Europe/Riga\",\"zip\":\"LV-4126\"}";
    private GeoLocation expectedLocation;

    @Before
    public void setUp() {

        expectedLocation = new GeoLocation();
        expectedLocation.setIp("185.61.150.209");
        expectedLocation.setCountry("Latvia");
        expectedLocation.setCity("Cēsis");
        expectedLocation.setRegion("Cesu Novads");
        expectedLocation.setZipCode("LV-4126");
        expectedLocation.setLatitude(57.3);
        expectedLocation.setLongitude(25.25);
    }

    // Mainly tests if the json response is correctly deserialized
    @Test
    public void testGetGeoLocationFromIp() {
        mockServer.expect(anything())
                .andRespond(withSuccess(JSON_LOCATION_RESPONSE, MediaType.APPLICATION_JSON));

        GeoLocation location = ipApiGeolocationService.getLocationFromIp(CESIS_IP);
        assertEquals("Location json not deserialized as expected", expectedLocation, location);
    }

    @Test
    public void testGetGeoLocationFromIpCache() {
        mockServer.expect(twice(), anything())
                .andRespond(withSuccess(JSON_LOCATION_RESPONSE, MediaType.APPLICATION_JSON));
        Cache cache = cacheManager.getCache("weather");
        cache.clear();
        assertThat(cache).isNotNull();
        GeoLocation location = ipApiGeolocationService.getLocationFromIp(CESIS_IP);
        assertEquals(location, cache.get(CESIS_IP).get());
    }


}
