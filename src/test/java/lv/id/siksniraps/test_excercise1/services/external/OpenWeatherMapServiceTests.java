package lv.id.siksniraps.test_excercise1.services.external;

import lv.id.siksniraps.test_excercise1.entities.GeoLocation;
import lv.id.siksniraps.test_excercise1.entities.WeatherForcast;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.ExpectedCount.twice;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(OpenWeatherMapService.class)
public class OpenWeatherMapServiceTests {

    @Autowired
    private OpenWeatherMapService openWeatherMapService;

    @Autowired
    private MockRestServiceServer mockServer;

    @Autowired
    private CacheManager cacheManager;

    private static final String JSON_WEATHER_RESPONSE = "{\"coord\":{\"lon\":25.25,\"lat\":57.3},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":17.26,\"pressure\":1012.45,\"humidity\":76,\"temp_min\":17.26,\"temp_max\":17.26,\"sea_level\":1020.11,\"grnd_level\":1012.45},\"wind\":{\"speed\":3.46,\"deg\":103.002},\"rain\":{\"3h\":10},\"snow\":{\"3h\":11},\"clouds\":{\"all\":56},\"dt\":1530552012,\"sys\":{\"message\":0.0135,\"country\":\"LV\",\"sunrise\":1530494911,\"sunset\":1530559033},\"id\":460570,\"name\":\"Cesis\",\"cod\":200}";
    private WeatherForcast expectedForcast;

    private GeoLocation location;

    @Before
    public void setUp() {
        expectedForcast = new WeatherForcast();
        expectedForcast.setCity("Cesis");
        expectedForcast.setDescription("broken clouds");
        expectedForcast.setClouds(56.0);
        expectedForcast.setCountry("LV");
        expectedForcast.setDateTime(new Date(1530552012L * 1000));
        expectedForcast.setHumidity(76.0);
        expectedForcast.setPressure(1012.45);
        expectedForcast.setTemperature(17.26);
        expectedForcast.setMaxTemperature(17.26);
        expectedForcast.setMinTemperature(17.26);
        expectedForcast.setLatitude(57.3);
        expectedForcast.setLongitude(25.25);
        expectedForcast.setRain(10.0);
        expectedForcast.setSnow(11.0);
        expectedForcast.setWindSpeed(3.46);
        expectedForcast.setWindDirection(103.002);

        location = new GeoLocation();
        location.setIp("185.61.150.209");
        location.setCountry("Latvia");
        location.setCity("Cēsis");
        location.setRegion("Cesu Novads");
        location.setZipCode("LV-4126");
        location.setLatitude(57.3);
        location.setLongitude(25.25);
    }

    // Mainly tests if the json response is correctly deserialized
    @Test
    public void testGetWeatherForcast() {
        mockServer.expect(anything())
                .andRespond(withSuccess(JSON_WEATHER_RESPONSE, MediaType.APPLICATION_JSON));

        WeatherForcast forcast = openWeatherMapService.getWeatherForcast(location);
        assertEquals("Weather forcast json not deserialized as expected", expectedForcast, forcast);
    }
}
